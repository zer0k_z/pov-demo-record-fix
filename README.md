## Demo Record Fix

SourceMod plugin that fixes the demo record command on KZ/Surf/Bhop servers.

**Warning:** Don't use if your server uses warmup period!
